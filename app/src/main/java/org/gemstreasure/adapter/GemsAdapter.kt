package org.gemstreasure.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import org.gemstreasure.GemsDescriptionActivity
import org.gemstreasure.R
import org.gemstreasure.model.GemsArray

class GemsAdapter(private val context: Context, private val arrayList: ArrayList<GemsArray>) :
        RecyclerView.Adapter<GemsAdapter.HolderItemGem>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderItemGem {
        val itemGem =
                LayoutInflater.from(parent.context).inflate(R.layout.model_gem, parent, false)
        return HolderItemGem(itemGem)
    }

    override fun onBindViewHolder(holder: HolderItemGem, position: Int) {
        val model: GemsArray = arrayList[position]
        holder.icon.setImageResource(model.iconImage)
        holder.name.text = model.name

        holder.icon.setOnClickListener {
            val intent = Intent(context, GemsDescriptionActivity::class.java)
            intent.putExtra("gemID", position)
            intent.putExtra("gemNAME", model.name)
            context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    class HolderItemGem(view: View) : RecyclerView.ViewHolder(view) {
        var icon: ImageView = view.findViewById(R.id.iv_iconImage)
        var name: TextView = view.findViewById(R.id.tv_iconName)
    }
}