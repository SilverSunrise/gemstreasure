package org.gemstreasure

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import org.gemstreasure.adapter.GemsAdapter
import org.gemstreasure.model.GemsArray

class MainActivity : AppCompatActivity() {

    private var recyclerView: RecyclerView? = null
    private var gridLayoutManager: GridLayoutManager? = null
    private var arrayList: ArrayList<GemsArray>? = null
    private var gemsAdapterGems: GemsAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        adapter()

    }

    private fun adapter() {
        recyclerView = findViewById(R.id.recyclerView)
        gridLayoutManager =
            GridLayoutManager(applicationContext, 2, LinearLayoutManager.VERTICAL, false)
        recyclerView?.layoutManager = gridLayoutManager
        recyclerView?.setHasFixedSize(true)
        arrayList = ArrayList()
        arrayList = setGemList()
        gemsAdapterGems = GemsAdapter(applicationContext, arrayList!!)
        recyclerView?.adapter = gemsAdapterGems
    }

    private fun setGemList(): ArrayList<GemsArray>? {

        val items: ArrayList<GemsArray> = ArrayList()
        items.add(GemsArray("Agate", R.drawable.agate))
        items.add(GemsArray("Amber", R.drawable.amber))
        items.add(GemsArray("Violet Diamond", R.drawable.argyle_violet))
        items.add(GemsArray("Blue Diamond", R.drawable.blue_diamonds))
        items.add(GemsArray("Crystal", R.drawable.crystal))
        items.add(GemsArray("Emerald", R.drawable.emerald))
        items.add(GemsArray("Jasper", R.drawable.jasper))
        items.add(GemsArray("Pink Diamond", R.drawable.pink_diamond))
        items.add(GemsArray("Sapphire", R.drawable.sapphire))
        items.add(GemsArray("Topaz", R.drawable.topaz))

        return items
    }
}