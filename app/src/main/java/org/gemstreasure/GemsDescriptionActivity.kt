package org.gemstreasure

import android.graphics.Typeface
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.RelativeSizeSpan
import android.text.style.StyleSpan
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_gems_description.*

class GemsDescriptionActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gems_description)
        val gemID: Int = intent.getIntExtra("gemID", 0)
        val gemName: String? = intent.getStringExtra("gemNAME")
        val styleSpan = StyleSpan(Typeface.BOLD)
        val sizeSpan = RelativeSizeSpan(1.2f)

        when (gemID) {
            0 -> {
                val spannableStringBuilder = SpannableStringBuilder(
                    "   What is Agate?\n\n Agate is a translucent variety of microcrystalline quartz. It is used as a semiprecious stone when it is of desirable quality and color.\n" +
                            "\n" +
                            "Agate generally forms by the deposition of silica from groundwater in the cavities of igneous rocks. The agate deposits in concentric layers around the walls of the cavity, or in horizontal layers building up from the bottom of the cavity. These structures produce the banded patterns that are characteristic of many agates. Some of these cavities are lined with crystals and those are known as geodes.\n" +
                            "\n" +
                            "Agate occurs in a wide range of colors, which include brown, white, red, gray, pink, black, and yellow. The colors are caused by impurities and occur as alternating bands within the agate. The different colors were produced as groundwaters of different compositions seeped into the cavity. The banding within a cavity is a record of water chemistry change. This banding gives many agates the interesting colors and patterns that make it a popular gemstone.\n\n" +
                            "Agates have been used as gemstones for thousands of years. They were some of the earliest stones fashioned by people. Today they are cut into cabochons, beads, small sculptures, and functional objects such as paperweights and bookends. Agate cabochons are popular and are used in rings, earrings, pendants, and other jewelry objects. Agate beads are commonly made into necklaces and earrings. Some have been used as marbles.\n\n " +
                            "Most agate has unimpressive colors and patterns. However, agate is a porous material that readily accepts dye. Most of the spectacularly colored agates sold in the gemstone trade have been dyed. Rarely, the color patterns of an agate form interesting landscape scenes. These are sought after by collectors."
                )

                spannableStringBuilder.setSpan(styleSpan, 3, 17, Spannable.SPAN_INCLUSIVE_EXCLUSIVE)
                spannableStringBuilder.setSpan(sizeSpan, 0, 14, Spannable.SPAN_INCLUSIVE_EXCLUSIVE)

                iv_descriptionNameIcon.setImageResource(R.drawable.agate)
                tv_gemName.text = gemName
                tv_gemDescription.text = spannableStringBuilder
            }
            1 -> {
                iv_descriptionNameIcon.setImageResource(R.drawable.amber)
                tv_gemName.text = gemName
                tv_gemDescription.text = (
                        "Amber is considered a gem because it glows and glistens when polished, but Amber is not actually a gemstone. It is the hardened resin of certain types of ancient trees that have been fossilized over millennia.\n" +
                                "\n" +
                                "Because of its unique properties, Amber has been adorned and studied relentlessly for centuries. It has been widely used for a variety of reasons including jewelry.\n" +
                                "\n" +
                                "Every Amber gemstone is unique and most likely mined from the Baltic region where most Amber deposits are found. It is estimated that there have been over 105 tons of Baltic Amber produced by the Palaeogene forests in Northern Europe. It is the largest known deposit of fossilized plant resin on earth.\n" +
                                "\n" +
                                "Baltic Amber, in lieu of Dominican Amber, is the highest quality for the preservation of fossil insects, something we'll get into a little later on.\n" +
                                "\n" +
                                "For now, here's a list of all the great things you didn't know about the Amber gemstone, that's not really a gemstone at all." +
                                "Surprisingly enough, Amber comes in a variety of luxurious colors. The most common color is the same as its name implies, a brownish honey color. But there are in fact 256 identified shades of Amber.\n" +
                                "\n" +
                                "If you find blue amber, it is most likely Dominican but Baltic Amber has been treated to have the same deep blue effect.\n" +
                                "\n" +
                                "Naturally, as we know this to be a form of resin, we expect the color ranges of yellow, orange, burnt red or copper colors and even sometimes cream or white. Some Amber may be green or even violet depending on the plant material that was originally preserved in the piece.\n" +
                                "Amber is classified as a gemstone for several different signs of the Zodiac for its strength and energy.\n" +
                                "\n" +
                                "It is the birthstone for the astrological sign of Cancer because it's been thought to reflect the energy of the sunniest and warmest month of the year in the Northern Hemisphere where it is found. Cancer falls from June 21 (the summer solstice) until July 22.\n" +
                                "\n" +
                                "You might also see it listed as a birthstone for the astrological sign of Taurus. It fits and aligns with this sign appropriately for the nature lover in the scope of its own earthy origins.\n" +
                                "\n" +
                                "The orange-gold colors in Amber are supposed to stabilize higher energies in the physical body and therefore influence Taurus' by calming them and offering protection and balance."
                        )
            }
            2 -> {
                iv_descriptionNameIcon.setImageResource(R.drawable.argyle_violet)
                tv_gemName.text = gemName.toString()
                tv_gemDescription.text =
                    "Violet Diamond has a violet complexion. She has an overall thin build, with the exception of her spherical upper and lower torsos. She possesses neon pink skin and dark purple, short hair, that has a big flick in the back. She has wide purple eyes with lilac pupils, violet eyeshadow above them, and a slightly pointed nose. She has large purple shoulder pads and white arm-length gloves. She has small plump lips and an elongated jaw. Her gemstone is embedded in her sternum.\n" +
                            "\n" +
                            "As for her outfit, Violet Diamond wears a pink and violet bodysuit, with large purple shoulder pads and white arm-length gloves. Her pants are divided into two colors and end in pink \"socks.\"\n " +
                            "Before the creation of Red Diamond, which took place 300 years after Pink Diamond's alleged demise, Violet Diamond emerged a defective Gem: she lacked the powers a Diamond needed to have. For this reason, the moment she formed she was subdued, poofed and bubbled by Yellow Diamond. While this situation was witnessed by a few high-class Gems around, Violet's name was buried away so that only Yellow herself and White Diamond knew she had ever existed.\n" +
                            "\n" +
                            "\n" +
                            "Violet Diamond in a bubble.\n" +
                            "\n" +
                            "Upon being bubbled away, Violet Diamond was converted into a Gem Key to power various Gem-tech and items, allowing for her to be used in a multitude of ways. Her gemstone was then locked away in one of Yellow's chambers.\n" +
                            "\n" +
                            "She never had a Pearl or a court.\n" +
                            "\n" +
                            "\"The Trial\" – \"Lars' Head\"\n" +
                            "Violet Diamond came close to being rescued by Ice and the Off Colors, but they ultimately failed for only a Diamond could enter the chamber where her gem was imprisoned.\n" +
                            "\n" +
                            "\"Legs From Here to Homeworld\" – \"Change Your Mind\"\n" +
                            "During her shenanigans on Homeworld, Violet Diamond's gem was managed to be taken hold by Ice. She fused with it, as Violet wouldn't reform, and together they formed a new being. Although the fusion didn't last long, Violet Diamond came out of her gem safe and sound.\n " +
                            "The fuss over violet and purple diamonds is recent as far as controversies go. Collectors of fancy color diamonds could only dream of owning these colors until the discovery of violet diamonds in Australia in the 1980s and a fluke find of purples in Russia a decade later. Yet even then, scientists doubted their eyes.\n" +
                            "Collecting and Classifying Fancy Coloured Diamonds refuses to recognize the violet diamond as a separate and distinct category. But it’s not because such stones are too purple. It’s because they’re too blue.\n" +
                            "Author Stephen Hofer says that at the time of the book’s publication in 1998 he had not yet found any diamonds that measured in the violet region of the spectrum using an electronic colorimeter. To him, most violet stones measured in the blue and were best classified as such."
            }
            3 -> {
                iv_descriptionNameIcon.setImageResource(R.drawable.blue_diamonds)
                tv_gemName.text = gemName.toString()
                tv_gemDescription.text =
                    "A blue diamond is a real, natural diamond with a noticeable blue tone due to the presence of boron in the diamond’s carbon structure. Blue diamonds range in color from light blue to deep blue often with a secondary hue like violet, gray, or green. Blue diamonds are not treated or enhanced to get their color, they are found below the earth’s surface with their natural blue coloring.\n\n " +
                            "Blue diamonds are found only in a few mines in the world: the Cullinan mine in South Africa, the Argyle Mine in Australia, and the Goloconda mine in India. The carat weight and intensity of blue color dictate how much a blue diamond is worth. Because of blue diamonds’ rarity, they’re usually more expensive than white diamonds and other fancy color diamonds.\n " +
                            "Blue diamonds are real, natural diamonds that form beneath the earth’s surface over billions of years. These diamonds are not color-treated or enhanced to get their blue color—it is natural. The mesmerizing color of the blue diamond comes from traces of boron in the diamond’s carbon composition.\n" +
                            "\n" +
                            "Though often confused, sapphires are entirely different gemstones than blue diamonds. Sapphires are a member of the corundum family and contain traces of titanium, iron, magnesium, copper and chromium.\n\n " +
                            "TYPES OF BLUE DIAMOND\n\n" +
                            "Most blue diamonds are Type IIb diamonds, which account for only 0.1% of the world’s color diamonds. There’s a spectrum within blue diamonds, though. They range from light to deep, vivid blue. Most diamonds also have a secondary hue that alters their color to be more green or gray, for example.\n" +
                            "\n" +
                            "Depending on the color intensity, blue diamonds look like pale blue gemstones, vibrant blue stones or a mix of two colors like blue and purple. Secondary hues add tint and character to the stone. No matter where a stone falls on the blue diamond color chart, it’s bound to be a stunning, rare gem.\n" +
                            "\n" +
                            "Blue diamonds belong to the family of Fancy Color Diamonds, the name for diamonds that exhibit a rich color. A blue diamond’s value is based on its 4Cs (Cut, Color, Clarity and Carat), with Color intensity being the most important factor.\n\n " +
                            "BLUE DIAMOND’S INTENSITY LEVELS\n\n" +
                            "The strength of a diamond’s color is referred to as the color intensity level. Each Fancy diamond has different intensity levels by which it’s evaluated. For blue diamonds, the grading scale includes Faint Blue, Very Light Blue, Light Blue, Fancy Light Blue, Fancy Blue, Fancy Intense Blue, Fancy Deep Blue and Fancy Vivid Blue. Fancy Dark is also a possibility if a secondary color is present.\n " +
                            "Every blue diamond is considered rare, but pure colors (ie. those without a secondary modifier) are even more unique. Pure blues are hard to obtain, however, and some color combinations are quite stunning. Gray-blue and green-blue are the most common secondary colors for blue diamonds but there are many color combination possibilities. This 0.46 Carat Fancy Gray Blue diamond is an example of a beautiful stone with a modifying color. Even stones like these are considered exceptional."
            }
            4 -> {
                iv_descriptionNameIcon.setImageResource(R.drawable.crystal)
                tv_gemName.text = gemName.toString()
                tv_gemDescription.text =
                    "Crystals are found everywhere. Most rocks are made of them, and most sandy beaches are actually billions of broken crystals of quartz. Quartz is a type of mineral, which is a substance found in rocks, soil, and sand. Even the sugar and salt in your kitchen are crystals. Some of the most beautiful crystals may be cut and polished to be used as gemstones (or “gems”). Gemstones are usually used in jewellery or as decoration. The loveliest and rarest gemstones are highly valuable and are often called precious gems. "
            }
            5 -> {
                iv_descriptionNameIcon.setImageResource(R.drawable.emerald)
                tv_gemName.text = gemName.toString()
                tv_gemDescription.text =
                    "Emerald is simply one of the most desirable, famous and historical gemstones of all time. Part of the Beryl family of gemstones, which also includes Aquamarine and Morganite, Emerald has been mined for around 4,000 years. From Ancient Egypt to the modern day, all those who have gazed on the intense vivid greens of the gem have fallen under its spell, and it can be found throughout time in some of the most stunning pieces of jewelry ever to have existed. Let's delve deeper into the fascinating history of this unmistakable treasure.\n\n " +
                            "The history of Emerald\n\n" +
                            "Emerald is simply the most famous green gemstone in the world. It holds an allure that has captivated people for thousands of years and inspires generation after generation of gemstone enthusiast. The stone has possibly the richest history, too. Pliny the Elder (23 - 79 AD) was mesmerized by this crystal, as was the Incan Empire in South America (1438 - 1533 AD) who had been using it in their jewelry for 500 years before trading with the 16th century explorers in order to obtain precious metals.\n\n " +
                            "This precious stone has been held in high regard since antiquity. The first known mines were in Egypt, and date back possibly as far as 4,000 BC, incredibly. Cleopatra was said to be hypnotized with the unique charm of this gemstone, and so adorned herself in the very finest Emeralds. The Greeks, who were working at the mines of Alexander the Great, were said to have yielded their gems to the Egyptian Queen too. In 1817, Cleopatra’s mines, which were once thought to be nothing but myth, were re-discovered on the coast of the Red Sea, adding significant credibility to her legend.\n\n " +
                            "Its name is derived from the Greek word ‘smaragdos’, a name once given to a number of gems with the color green in common. The world evolved over time to the current ‘Emerald’, and the given name ‘Esmerelda’ also translates to Emerald. Representative of the color of spring, Emeralds are said to signify hope, new growth and eternal life. The intense color of the gemstone has long been associated with the lushest of landscapes. For example, Ireland is known as the Emerald Isle and Seattle as the Emerald City. The association has even crossed over into works of fiction, such as the Emerald City in The Wizard of Oz.\n\n" +
                            "Pliny the Elder, the author of ‘Natural History’, wrote of Emerald “...no color is more delightful in appearance. For although we enjoy looking at plants and leaves, we regard Emeralds with all the more pleasure because compared with them there is nothing that is more intensely green”. Although not a believer in myths, Pliny did go on to say, “And after straining our eyes by looking at another object, we can restore our vision to normal by gazing at an Emerald”. He also correctly identified Emeralds as part of the Beryl family. "
            }
            6 -> {
                iv_descriptionNameIcon.setImageResource(R.drawable.jasper)
                tv_gemName.text = gemName.toString()
                tv_gemDescription.text =
                    "The appeal of Jasper is its interesting color patterns and formations. Though it can be a solid color, it is most often mottled, spotted, ringed, or striped. Each Jasper has a unique color or pattern, lending this gemstone much variety. Jasper is an ancient gemstone, and is mentioned in the bible and other classical sources. Though fairly common and affordable today, Jasper in antiquity was regarded as a valuable stone.\n\n " +
                            "Jasper is generally an inexpensive gemstone when used in jewelry. It is cut and polished into cabochons, and used as beads for necklaces and bracelets. It is also carved into cameos which can be worn as pendants.\n " +
                            "Jasper has an over-abundance of variety names. Some variety names are generally used by collectors and dealers, but there are many made up by dealers to describe a locality or other habit. The varieties below are the well-known names or varieties that are commonly encountered. Seldom-used and localized trade names are not described here.\n " +
                            "Most Jasper is natural and not treated or enhanced, although occasionally it is dyed.\n " +
                            "Jasper is common and found worldwide. Important deposits exist in India, Russia, Kazakhstan, Indonesia, Egypt, Madagascar, Australia, Brazil, Venezuela, Uruguay and the United States (Oregon, Idaho, Washington, California, Arizona, Utah, Arkansas, and Texas).\n " +
                            "Jasper is distinguished from other Chalcedony varieties such as Agate and Carnelian by its opacity. The unique color patterns (combined with hardness) can distinguish Jasper from all other gemstones.\n\n " +
                            "Agate Jasper  -   Opaque multicolored Jasper, or Jasper with banding; may also refer to a single stone with a combination of both Agate and Jasper.\n\n" +
                            "Biggs Jasper  -  Jasper from Biggs Junction, Oregon, with varying light and dark color brown bands and pretty formations.\n\n" +
                            "Brecciated Jasper  -   Jasper in rounded fragments naturally cemented together in a gray material; appears similar to breccia.\n\n" +
                            "Bruneau Jasper  -  Jasper from Bruneau Canyon, in Owyhee County, Idaho, with distinctive brown, cream, (and sometimes even red or green) banding and patterns.\n\n" +
                            "Cave Creek Jasper  -   Reddish Jasper found near Cave Creek in Maricopa County, Arizona."
            }
            7 -> {
                iv_descriptionNameIcon.setImageResource(R.drawable.pink_diamond)
                tv_gemName.text = gemName.toString()
                tv_gemDescription.text =
                    "Rare and awe inspiring, the pink diamond is one of the most valuable coloured diamonds in the world. Worth anywhere between 10 to 100 times more than an equivalent white, pink diamonds represent only 0.03% of the global annual production of diamonds.\n" +
                            "\n" +
                            "Coloured diamonds, whether blue, yellow, orange or green, are more valuable than an equivalent white. But when it comes to pinks, they are so exceptional that you could fit into the palm of your hand an entire year's yield of premium-quality pink diamonds. And that makes them the most portable form of wealth on our planet.\n" +
                            "\n" +
                            "So few and far between are top-quality pink diamonds that to commemorate Queen Elizabeth's Silver Jubilee in 2012, the Argyle Pink Diamonds company held an exhibition in London, for one day only. Calling on jewellers and collectors from around the world, it provided a sight unlikely to be witnessed again in our lifetimes. The 42 gems, valued at around £40 million, represented the greatest coming together in one place of pink diamonds. Experts from around the world were awestruck at the sight of so many of these rare stones under one roof.\n" +
                            "\n" +
                            "The desirability of pink diamonds is reflected in the spiralling prices they achieve in auction rooms around the world. The record for the highest price paid per carat for a pink diamond was set at Sotheby's in Hong Kong on 7 October 2014 when a 8.41 carat Fancy Vivid Purple-Pink Internally Flawless sold for US\$17,768,041or \$2,112,727 per carat. The unusual shade of purple-tinged pink made this diamond highly covetable. What's more, it has been graded by the Gemological Institute of America as Internally Flawless, which is very unusual for a coloured diamond.This level of purity is normally only found in white diamonds. De Beers cut the stone from a 19.54 carat rough and maximised the intensity of colour of the final cut. \n" +
                            "\n" +
                            "This stunning pink diamond broke the previous record for the highest price paid per carat for a pink diamond, which was held by the 5.00 carat Fancy Vivid Pink that sold in 2009 for US\$10 million, or \$2 million per carat. The record overall price paid for a pink diamond belongs to what is now known as the \"Graff Pink\", which was bought by jeweller Laurence Graff at Sotheby's auction in November 2010. The jaw-droppingly large 24.78 carat Fancy Intense emerald-cut pink diamond sold for US\$46.2 million, or just over \$1.6 million per carat. \n" +
                            "\n" +
                            "Read about Sotheby's Unique Pink diamond\n" +
                            "\n" +
                            "And it seems that for pink diamonds, it is record breaker after record breaker. In 2012,Christie's in New York sold a 9.00 carat pink diamond ring from the turn-of-the-century from the estate of Huguette M. Clark for \$15.7 million, making it the most valuable pink diamond ever sold at auction in America.  \n" +
                            "\n" +
                            "So when the world's wealthiest seek out something to truly set them apart, a pink diamond is the gemstone of choice. From Queen Elizabeth to the Sultan of Brunei and Hollywood stars, they are all drawn to the allure of the pink diamond. Jennifer Lopez's 6.00 carat pink diamond engagement ring from Ben Affleck is reported to have set him back a cool \$1.2 million. Tennis player Anna Kournikova's pink diamond engagement ring, with a pear-cut centre stone from the Argyle Mine, has an estimated value of over \$2.5 million.\n" +
                            "\n" +
                            "Curiously, pink diamonds are always referred to as \"she\" and historically the Darya-I-Noor - Sea of Light - of the Iranian Crown Jewels is the largest pink diamond in the world. She is followed by the Noor-ol-Ain - Eye of Light - weighing 60.00 carats, believed to have come from the now depleted ancient and famed Golconda mines in India. Harry Winston was commissioned to set it into the Empress Farah's of Iran's wedding tiara in 1959. In 1999, a pink diamond was discovered by De Beers in South Africa weighing 132.5 carat in the rough. Cut by De Beers and sold to Steinmetz, who renamed it the Steinmetz Pink, the 59.60 carat oval-cut pink diamond is both internally flawless and a much-prized Fancy Vivid Pink colour. \n" +
                            "\n" +
                            "Queen Elizabeth II, one of the greatest jewellery collectors and lover of diamonds, owns the Williamson pink diamond brooch, which she can often be seen wearing at State functions. The 23.60 carat pink diamond at the centre of the brooch is believed to be one of the finest ever discovered. The stone was found in 1947 at the Mwadui mine in Tanganyika, Africa. The owner of the mine, Dr John Thorburn Williamson, gave it to Princess Elizabeth for her wedding and, in 1953, Cartier in London set the diamond into a flower-shaped brooch. \n" +
                            "\n" +
                            "The increasing rarity of pink diamonds only adds to their allure. So much so that only about 50 or 60 top-quality stones come on to the market every year. But don't expect to see conker-sized pinks - most are no bigger than a pea. If that does sound disappointingly small, when a single carat can fetch almost a million pounds, it is clear anything over one carat is exceptional.\n" +
                            "\n" +
                            "Of course, smaller, lesser-quality pinks in lighter tones exist in greater numbers, but they are still many times more valuable than your average white. The reason there are few large top-quality pink diamonds is likely due to the force of the subterranean volcanic explosions that forced the diamonds nearer to the Earth's surface and shattered many on the way. A top-quality pink diamond is desirable; a large top-quality pink diamond of over 2.00 carats is exponentially more so.\n" +
                            "\n" +
                            "As with all coloured diamonds, tone and saturation is everything. Colours vary from candyfloss pink to rich cherry jam hues. Vivid hues are right at the end of the colour spectrum of pink tones and are the most expensive.The rule of thumb is, the more intense the colour, the more valuable the stone. The most intense are rated as \"Fancy Vivid\", culminating in the highly valuable red diamond, which is rarer still than the pinks.\n" +
                            "\n" +
                            "The three most important factors determining the colour and therefore the value of a pink diamond are the hue - which is the dominant colour of the diamond - the tone - the amount of light or darkness in the stone - and the saturation - the intensity of the hue. If a pink diamond achieves a top grading in all three categories the result is exceptional, even if the stone is no bigger than a drop of water. Japan is the strongest market for pink diamonds, where the lighter \"cherry blossom\" shades are favoured.\n" +
                            "\n" +
                            "But how did pink diamonds come about? Josephine Johnson, Manager of Argyle Pink Diamonds, tells us: \"We know pink diamonds are 1.6 billion years old, but we are not sure how they came to have their colour. What we do know is that all diamonds are produced under great pressure and pink diamonds even more so.This is caused by a slight twist in the crystal lattice, which makes the light refract through the diamond, and it comes out in the far end of the colour spectrum in colours such as pinks and reds.\"\n" +
                            "\n" +
                            "Until the Argyle Diamond Mine in the remote region Western Australia was discovered in the late 1970s and started yielding pink diamonds, they were such an oddity that they were the territory of wealthy collectors of curios. This rare geological freak of nature was randomly found in Brazil, Russia and India and was more likely to be seen in an exhibition of minerals in a museum than gracing a pair of earrings. Jewellers were not entirely sure of their value so were hesitant about investing in them. What's more, it was difficult to find matching stones to make anything other than a solitaire ring, and a pink diamond was more likely to be hidden away in a bank vault than appear on the finger of a diva. \n" +
                            "\n" +
                            "But the discovery of the Argyle Diamond Mine in Australia changed the course of the history of pink diamonds. After prospecting in the Barramundi Gap in Northwestern Australia, two very determined geologists spotted a diamond shimmering in an anthill. The industrious ants had been digging up diamonds and had quite literally brought them to light. The Aboriginals had been aware of their presence and believed that the colours of the diamonds come from the scale of the barramundi fish, with pink diamonds coming from the heart."
            }
            8 -> {
                iv_descriptionNameIcon.setImageResource(R.drawable.sapphire)
                tv_gemName.text = gemName.toString()
                tv_gemDescription.text =
                    "The most valuable color of Sapphire is a cornflower blue color, known as Kashmir Sapphire or Cornflower Blue Sapphire. Another extremely valuable Sapphire form is the very rare, orange-pink Padparadschah. An exotic type of sapphire, known as Color Changing Sapphire, displays a different color depending on its lighting. In natural light, Color Changing Sapphire is blue, but in artificial light, it is violet. (This effect is the same phenomenon well-known in the gemstone Alexandrite). Yellow and pink Sapphire have recently become very popular, and are now often seen in jewelry.\n" +
                            "\n" +
                            "Going way back in time, Sapphires (excluding blue) were often called the same name as a popular gemstone of that color with the prefix \"oriental\" added to it. For example, green Sapphire was called \"Oriental Emerald\". The practice of applying the name of a different gemstone to identify the sapphire was misleading, and these names are no longer used. What was once called \"Oriental Emerald\" is now called \"Green Sapphire\". The same holds true for all other color varieties of Sapphire. However, the word \"Sapphire\" in its plain context refers only to blue Sapphire, unless a prefix color is specified. Sapphire with a color other than blue is often called a \"fancy\" in the gem trade.\n" +
                            "\n" +
                            "Sapphire often contains minor inclusions of tiny slender Rutile needles. When present, these inclusions decrease the transparency of a stone and are known as silk. When in dense, parallel groupings, these inclusions can actually enhance by allowing polished Sapphires to exhibit asterism. Sapphire gems displaying asterism are known as \"Star Sapphire\", and these can be highly prized. Star Sapphire exists in six ray stars, though twelve ray stars are also known.\n" +
                            "\n" +
                            "Sapphire is pleochroic, displaying a lighter and more intense color when viewed at different angles. Some pleochroic Sapphire is blue when viewed at one angle, and purple at a different angle. Color zoning, which forms from growth layers that build up during the formation of the stone, may also be present in certain Sapphires. Color zoning is responsible for certain Sapphires having lighter and darker colors in different parts of a crystal. Some Sapphire gemstones may even be multicolored such as purple and blue.\n" +
                            "\n" +
                            "Sapphire is a tough and durable gem, and the only natural gemstone harder than Sapphire is Diamond. Despite this, Sapphire is still subject to chipping and fracture if handled roughly, and care should be taken to ensure it is properly handled. Sapphire was first synthesized in 1902. The process of creating synthetic Sapphire is known as the Verneuil process. Only experts can distinguish between natural and synthetic Sapphire.\n " +
                            "Sapphire is one of the most popular gemstones, and is used extensively in Jewelry. Fine colored Sapphire with a deep blue color and excellent transparency can reach several thousand dollars a carat. The blue variety is most often used in jewelry, but the yellow, pink, and orange \"fancies\" have recently become very popular. Green and light blue Sapphires are also known, but are less commonly used in jewelry. Opaque Black Sapphire is also used a minor gemstone.\n" +
                            "\n" +
                            "Sapphire is used in all forms of jewelry, including bracelets, necklaces, rings, and earrings. It is used both as centerpiece gemstone in pendants and rings, as well as a secondary stone to complement other gemstones such as Diamonds. Star Sapphires are polished as cabochons, and, if clear, are extremely valuable.\n" +
                            "\n" +
                            "The rare orange-pink variety, known as Padparadschah, can be even more valuable than fine blue Sapphire. Blue Sapphire is sometimes carved into cameos or small figures, especially the less transparent material. Synthetic Sapphire is often used as a cheap substitute for the natural material.\n" +
                            "\n" +
                            "Sapphire is the birthstone of September.\n " +
                            "Besides for the varieties of Sapphire listed below, Sapphire with color other than blue are prefixed with their color names. The main gemstone colors in addition to blue Sapphire include:\n" +
                            "- Yellow Sapphire (sometimes also called \"Golden Sapphire\" if intensely colored)\n" +
                            "- Pink Sapphire\n" +
                            "- White Sapphire (describes Sapphire that is colorless)\n" +
                            "- Green Sapphire\n" +
                            "- Purple Sapphire\n" +
                            "- Orange Sapphire\n" +
                            "- Black Sapphire"
            }
            9 -> {
                iv_descriptionNameIcon.setImageResource(R.drawable.topaz)
                tv_gemName.text = gemName.toString()
                tv_gemDescription.text = "Topaz is a rare silicate mineral with a chemical composition of Al2SiO4(F,OH)2. It is best known for three things: 1) being one of the world's favorite colored gemstones; 2) being \"mineral number 8\" in the Mohs Scale of Hardness; and, 3) being a birthstone for the month of November.\n" +
                        "\n" +
                        "Topaz obtains much of its popularity from its beautiful colors and its status as a birthstone. Natural topaz colors include rare and valuable yellow, orange, pink, red, purple, and blue. The most affordable and frequently purchased is blue topaz that has received its color from treatment.\n" +
                        "\n" +
                        "In 1812, Friedrich Mohs developed a set of 10 standard minerals that could be used to assign a relative hardness to minerals and other materials. Today, over 200 years later, his hardness scale is used by students and geologists throughout the world to identify mineral specimens.\n" +
                        "\n" +
                        "Topaz - The November Birthstone\n\n" +
                        "Topaz is the original modern birthstone for the month of November. Its position as a birthstone contributes to the gem's popularity. Topaz jewelry can be found for sale in almost every jewelry store.\n" +
                        "\n" +
                        "In 1952, Jewelers of America made a few modifications to the list of modern birthstones. That was when citrine, a yellow to orange to reddish brown variety of quartz, was added as a second birthstone for the month of November.\n\n " +
                        "One of the best-known physical properties of topaz is its hardness. It has a hardness of 8 on the Mohs hardness scale, making it the hardest silicate mineral. It also serves as the Mohs hardness scale index mineral for a hardness of 8. Every student who takes a physical geology course learns about the hardness of topaz. Diamond, corundum, and chrysoberyl are the only commonly known minerals that are harder.\n" +
                        "\n" +
                        "Topaz occurs in a wide range of colors. The most valuable colors for use in jewelry are natural pink, orange, red, purple and blue. These colors are very rare.\n" +
                        "\n" +
                        "The most common natural colors are colorless, pale yellow, and brown. While these colors are not important for jewelry use in their natural state, they can be treated in a variety of ways to produce colors that are much more desirable.\n" +
                        "\n" +
                        "When allowed to grow in an unrestricted cavity, topaz forms orthorhombic crystals, often with striations that parallel the long axis of the crystal. It also has a distinct basal cleavage that breaks to form vitreous fracture surfaces perpendicular to the long axis of the crystal. This cleavage makes topaz a more fragile gemstone than its hardness of 8 would imply. Topaz is very hard, but it is also brittle and cleaves easily.\n" +
                        "\n" +
                        "Topaz has a specific gravity that ranges between 3.4 and 3.6. This is quite high for a mineral composed of aluminum, silicon, and gaseous elements. This high specific gravity causes it to be concentrated into placer deposits by stream currents.\n " +
                        "What Color is Topaz?\n\n" +
                        "Topaz occurs in a wide range of natural colors; however, most natural topaz is colorless. The most highly regarded colors are the reds and pinks, which receive their color from trace amounts of chromium. Chromium is also responsible for the color in violet and purple topaz.\n" +
                        "\n" +
                        "A variety known as \"imperial topaz\" is especially valuable because people enjoy its reddish orange to orangy red colors, which often both occur in the same crystal. Most of the world's imperial topaz is found in Brazil. Topaz with a natural blue color is very rare and valuable.\n" +
                        "\n" +
                        "Yellow, brown, and colorless topaz have lower values. These colors are often heated, irradiated, coated, and treated in other ways to alter their color."
            }
        }
    }
}